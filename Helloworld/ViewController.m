//
//  ViewController.m
//  Helloworld
//
//  Created by Mark Farrell on 1/12/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIScrollView* sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor blackColor];
    [sv setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    [self.view addSubview:sv];
    
    
    
    
    
    int height = self.view.frame.size.height;
    int width = self.view.frame.size.width;
    
    for (int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
        UIView* view = [[UIView alloc]initWithFrame:(CGRectMake(j * width, i * height, width, height))];
                        switch (i) {
                            case 0:
                                if(j == 0){
                                    view.backgroundColor = [UIColor redColor];
                                    UILabel* lbl1 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl1];
                                    lbl1.text = [NSString stringWithFormat: @"Page 1"];
                                }
                                else if(j == 1){
                                    view.backgroundColor = [UIColor whiteColor];
                                    UILabel* lbl2 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl2];
                                    lbl2.text = [NSString stringWithFormat: @"Page 2"];
                                }
                                else if(j == 2){
                                    view.backgroundColor = [UIColor redColor];
                                    UILabel* lbl3 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl3];
                                    lbl3.text = [NSString stringWithFormat: @"Page 3"];
                                }
                                break;
                            case 1:
                                if(j == 0){
                                    view.backgroundColor = [UIColor greenColor];
                                    UILabel* lbl4 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl4];
                                    lbl4.text = [NSString stringWithFormat: @"Page 4"];
                                }
                                else if(j == 1){
                                    view.backgroundColor = [UIColor yellowColor];
                                    UILabel* lbl5 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl5];
                                    lbl5.text = [NSString stringWithFormat: @"Page 5"];
                                }
                                else if(j == 2){
                                    view.backgroundColor = [UIColor greenColor];
                                    UILabel* lbl6 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl6];
                                    lbl6.text = [NSString stringWithFormat: @"Page 6"];
                                }
                                break;
                            case 2:
                                if(j == 0){
                                    view.backgroundColor = [UIColor redColor];
                                    UILabel* lbl7 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl7];
                                    lbl7.text = [NSString stringWithFormat: @"Page 7"];
                                }
                                else if(j == 1){
                                    view.backgroundColor = [UIColor whiteColor];
                                    UILabel* lbl8 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl8];
                                    lbl8.text = [NSString stringWithFormat: @"Page 8"];
                                }
                                else if(j == 2){
                                    view.backgroundColor = [UIColor redColor];
                                    UILabel* lbl9 = [[UILabel alloc]initWithFrame:(CGRectMake(100, 100, 100, 100))];
                                    [view addSubview:lbl9];
                                    lbl9.text = [NSString stringWithFormat: @"Page 9"];
                                }
                                break;
                            default:
                                break;
                        }
        [sv setPagingEnabled:(YES)];
        [sv addSubview:(view)];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
